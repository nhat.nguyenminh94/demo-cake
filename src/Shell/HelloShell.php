<?php
namespace App\Shell;

use Cake\Console\Shell;

class HelloShell extends Shell
{
    public function main()
    {
        $this->out('Hello Cat.');
    }
    public function heyThere($name = 'Anonymous')
    {
        $this->out('Hey there ' . $name);
    }
}