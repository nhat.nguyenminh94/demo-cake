<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('style.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <?php echo $this->Html->script('http://code.jquery.com/jquery.min.js'); ?>
    <style>
        label{
            color: red;
        }
    </style>
</head>
<body>
    <nav class="top-bar expanded" data-topbar role="navigation">
        <ul class="title-area large-3 medium-4 columns">
            <li class="name">
                <h1><?= $this->Html->link(__('Product'), ['controller' => 'Products','action' => 'index']) ?></a></h1>

            </li>
            <li class="name">
                <h1><?= $this->Html->link(__('Category_title'), ['controller' => 'Categories','action' => 'index']) ?></h1>
            </li>
        </ul>
        <div class="top-bar-section">
            <ul class="right">
                <li>
                    <?php
                        echo $this->Form->create(null, [
                            'url' => ['controller' => 'Localizations', 'action' => 'index']
                        ]);
                        echo $this->Form->radio("locale",[
                            ['value'=>'en_US','text'=>'English',$lang == 'en_US' ? "checked" : ''],
                            ['value'=>'vi','text'=>'Việt Nam',$lang == 'vi' ? "checked" : ''],
                        ],['class'=>'radio-btn']);
                        echo $this->Form->button(__('Change_language'));
                        echo $this->Form->end();
                    ?>
                </li>
            </ul>
        </div>
    </nav>
    <?= $this->Flash->render() ?>
    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>
    <footer>
    </footer>
</body>
</html>
