<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

/**
 * Math component
 */
class MathComponent extends Component
{

    protected $_defaultConfig = [];
    public function doComplexOperation($amount1, $amount2)
    {
        return $amount1 + $amount2;
    }
}
