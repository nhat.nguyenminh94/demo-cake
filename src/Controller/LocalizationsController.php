<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\I18n\I18n;
use Cake\Http\Cookie;
class LocalizationsController extends AppController{
    public function index()
    {
        if($this->request->is('post')){
            $locale = $this->request->data('locale');
            I18n::setLocale($locale);
            $this->Cookie->write('web_lang', $locale);
//            return $this->redirect("/");
            return $this->redirect($this->request->referer());
        }

    }

}
?>