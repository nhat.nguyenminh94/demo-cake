<?php
use Migrations\AbstractSeed;

/**
 * CategoriesSeeder seed.
 */
class CategoriesSeederSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
//        $data = [];
//
//        $faker = Faker\Factory::create();
////        for ($i = 0; $i < 5; $i++) {
//            $data[] = [
//                'name'      => $faker->name,
//                'created'       => date('Y-m-d H:i:s'),
//                'modified'       => date('Y-m-d H:i:s'),
//            ];
//        }
//        $this->insert('categories', $data);
        $data = [
            [
                'name'    => 'Bia',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'name'    => 'Nước Ngọt',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'name'    => 'Nước Trái Cây',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ]
        ];
        $table = $this->table('categories');
        $table->insert($data)->save();

    }
}
