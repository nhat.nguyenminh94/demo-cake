<?php
use Migrations\AbstractSeed;

/**
 * ProductsSeeder seed.
 */
class ProductsSeederSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name'    => 'Bia 333',
                'description'    => 'Detail 01',
                'category_id'    => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'name'    => 'Coca',
                'description'    => 'Detail 02',
                'category_id'    => 2,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'name'    => 'Nước Cam',
                'description'    => 'Detail 03',
                'category_id'    => 2,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'name'    => 'Pepsi',
                'description'    => 'Detail 04',
                'category_id'    => 2,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ]

        ];
        $table = $this->table('products');
        $table->insert($data)->save();
//        $faker = Faker\Factory::create();
//        $data = [];
//        for ($i = 0; $i < 10; $i++) {
//            $data[] = [
//                'name'      => $faker->name,
//                'description'      => $faker->text,
//                'category_id'     => $faker->randomDigitNotNull,
//                'created'       => date('Y-m-d H:i:s'),
//                'modified'       => date('Y-m-d H:i:s'),
//            ];
//        }
//        $this->insert('products', $data);

    }
}
